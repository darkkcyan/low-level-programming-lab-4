#ifndef __CALLBACK_UTILS_H__
#define __CALLBACK_UTILS_H__

#include <stdbool.h>

void write_int_to_text_file(int num, void* text_file);
int read_int_from_text_file(void* text_file, bool* stop);
void serialize_int_to_file(int num, void* file);
int deserialize_int_from_file(void* file, bool* stop);

#endif
