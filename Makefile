CC = gcc
FLAGS = -g -std=c99 -pedantic-errors -Wall -O2

all: main
	
clean:
	rm *.o
	rm main
	
linked-list.o: linked-list.h linked-list.c
	$(CC) $(FLAGS) -c linked-list.c 

callback-utils.o: callback-utils.h callback-utils.c
	$(CC) $(FLAGS) -c callback-utils.c
	
linked-list-utils.o: linked-list-utils.h linked-list-utils.c callback-utils.o
	$(CC) $(FLAGS) -c linked-list-utils.c
	
main.o: main.c
	$(CC) $(FLAGS) -c main.c
	 
main: main.o linked-list.o callback-utils.o linked-list-utils.o
	$(CC) $(FLAGS) main.o linked-list.o callback-utils.o linked-list-utils.o -o main 
