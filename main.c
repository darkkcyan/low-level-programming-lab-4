#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include "linked-list.h"
#include "linked-list-utils.h"
#include "callback-utils.h"

// global linked_list
struct linked_list_node* g_list = NULL;

enum CommandRetValue {
    CMD_SUCCESS, CMD_INVALID_ARGUMENT, CMD_INVALID_INDEX
};

typedef int (commandHandler)(const char* arg_line);

// functions for commands
static int cmd_help(const char* arg_line);

static int cmd_quit(const char* arg_line) {
    list_free(g_list);
    exit(0);
    return CMD_SUCCESS;
}

static int cmd_print_list(const char* arg_line) {
    printf("List size: %zu\n", list_length(g_list));
    foreach_with(g_list, stdout, write_int_to_text_file);
    printf("\n");
    return CMD_SUCCESS;
}

static int cmd_add_back(const char* arg_line) {
    int val;
    if (sscanf(arg_line, "%*s %d", &val) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    list_add_back(&g_list, val);
    return CMD_SUCCESS;
}

static int cmd_add_front(const char* arg_line) {
    int val;
    if (sscanf(arg_line, "%*s %d", &val) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    list_add_front(&g_list, val);
    return CMD_SUCCESS;
}

static int cmd_erase(const char* arg_line) {
    size_t index;
    if (sscanf(arg_line, "%*s %zu", &index) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    size_t length = list_length(g_list);
    if (index >= length) {
        return CMD_INVALID_INDEX;
    }
    struct linked_list_node* erasing_node;
    if(index == length - 1) {
        erasing_node = g_list;
        g_list = g_list->next;
    } else {
        struct linked_list_node* prv = list_node_at(g_list, index + 1);
        erasing_node = prv->next;
        prv->next = erasing_node->next;
    }
    erasing_node->next = NULL;
    list_free(erasing_node);
    return CMD_SUCCESS;
}

static int cmd_clear(const char* arg_line) {
    list_free(g_list);
    g_list = NULL;
    return CMD_SUCCESS;
}

static int cmd_sum(const char* arg_line) {
    printf("%d\n", list_sum(g_list));
    return CMD_SUCCESS;
}

static int min(int u, int v) {
    return u < v ? u : v;
}

static int max(int u, int v) {
    return u > v ? u : v;
}

static int cmd_min(const char* arg_line) {
    printf("%d\n", foldl(g_list, INT_MAX, min));
    return CMD_SUCCESS;
}

static int cmd_max(const char* arg_line) {
    printf("%d\n", foldl(g_list, INT_MIN, max));
    return CMD_SUCCESS;
}

static int cmd_change(const char* arg_line) {
    size_t index;
    int value;
    if (sscanf(arg_line, "%*s %zu %d", &index, &value) != 2) {
        return CMD_INVALID_ARGUMENT;
    }
    struct linked_list_node* where = list_node_at(g_list, index);
    if (!where) {
        return CMD_INVALID_INDEX;
    }
    where->value = value;
    return CMD_SUCCESS;
}

static int inc(int value, void* other) {
    return value + *((int*)other);
}
static int cmd_map_inc(const char* arg_line) {
    int num;
    if (sscanf(arg_line, "%*s %d", &num) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    map_mut_with(g_list, &num, inc);
    return CMD_SUCCESS;
}

static int mul(int value, void* other) {
    return (int)(value * *((float*)other));
}

static int cmd_map_mul(const char* arg_line) {
    float num;
    if (sscanf(arg_line, "%*s %f", &num) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    map_mut_with(g_list, &num, mul);
    return CMD_SUCCESS;
}

static int cmd_save(const char* arg_line) {
    char filename[255];
    if (sscanf(arg_line, "%*s %s", filename) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    if (save(g_list, filename)) {
        printf("Successfully writed the list to the text file \"%s\".\n", filename);
        return CMD_SUCCESS;
    } else {
        printf("Failed to write the list to the text file \"%s\".\n", filename);
        return CMD_SUCCESS;
    }
}

static int cmd_load(const char* arg_line) {
    char filename[255];
    if (sscanf(arg_line, "%*s %s", filename) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    struct linked_list_node* res = NULL;
    if (load(&res, filename)) {
        printf("Successfully loaded the list from the text file \"%s\".\n", filename);
        list_free(g_list);
        g_list = res;
        return CMD_SUCCESS;
    } else {
        printf("Failed to load the list from the text file \"%s\".\n", filename);
        return CMD_SUCCESS;
    }
}

static int cmd_serialize(const char* arg_line) {
    char filename[255];
    if (sscanf(arg_line, "%*s %s", filename) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    if (serialize(g_list, filename)) {
        printf("Successfully serialized the list to the file \"%s\".\n", filename);
        return CMD_SUCCESS;
    } else {
        printf("Failed to serialize the list to the file \"%s\".\n", filename);
        return CMD_SUCCESS;
    }
}

static int cmd_deserialize(const char* arg_line) {
    char filename[255];
    if (sscanf(arg_line, "%*s %s", filename) != 1) {
        return CMD_INVALID_ARGUMENT;
    }
    struct linked_list_node* res = NULL;
    if (deserialize(&res, filename)) {
        printf("Successfully deserialized the list from the file \"%s\".\n", filename);
        list_free(g_list);
        g_list = res;
        return CMD_SUCCESS;
    } else {
        printf("Failed to deserialize the list from the file \"%s\".\n", filename);
        return CMD_SUCCESS;
    }
}

struct Command {
    char* name;
    char* arguments_name;
    commandHandler* handler;
    char* description;
};

struct Command commands[] = {
    {"help", "", cmd_help, "Print this message"},
    {"print", "", cmd_print_list, "Print all elements of the list"},
    {"add-back", "num", cmd_add_back, "Add a new number to the back of the list"},
    {"add-front", "num", cmd_add_front, "Add a new number to the front of the list"},
    {"erase", "index", cmd_erase, "Erase element at index"},
    {"change", "index num", cmd_change, "Change the value of the element at the position index"},
    {"clear", "", cmd_clear, "Clear the list"},
    {"save", "filename", cmd_save, "Save the list to a text file"},
    {"load", "filename", cmd_load, "Load the list to a text file"},
    {"serialize", "filename", cmd_serialize, "Serialize the list to a binary file"},
    {"deserialize", "filename", cmd_deserialize, "Deserialize the list from a binary file"},
    {"sum", "", cmd_sum, "Print the sum of all elements"},
    {"min", "", cmd_min, "Print the min of all elements, or INT_MAX if there is no elements"}, 
    {"max", "", cmd_max, "Print the max of all elements, or INT_MIN if there is no elements"}, 
    {"map-inc", "number", cmd_map_inc, "Increase all elements by number. Number can be negative"},
    {"map-mul", "number", cmd_map_mul, "Multiply all elements by number, then round the result down. Number can be a float"},
    {"quit", "", cmd_quit, "Free the list and exit"},
};

const size_t COMMAND_COUNT = sizeof(commands) / sizeof(commands[0]); 

static int cmd_help(const char* arg_line) {
    const char* format = "  %15s %-15s%s\n";
    printf(format, "Command name", "Command args", "Description");
    printf(format, "------------", "------------", "-----------");
    for (int i = 0; i < COMMAND_COUNT; ++i) {
        printf(format, commands[i].name, commands[i].arguments_name, commands[i].description);
    }
    return CMD_SUCCESS;
}

int main(int argc, char** argv) {
#define COMMAND_STR_BUFF_SIZE 300
    static char command_str[COMMAND_STR_BUFF_SIZE];
    static char command_name[COMMAND_STR_BUFF_SIZE];
    
    while (true) {
        printf("> ");
        if (!fgets(command_str, COMMAND_STR_BUFF_SIZE, stdin)) {
            break;
        }
        sscanf(command_str, "%s", command_name);
        size_t command_id = 0;
        while (command_id < COMMAND_COUNT && strcmp(command_name, commands[command_id].name) != 0) {
            ++command_id;
        }
        if (command_id == COMMAND_COUNT) {
            fprintf(stderr, "Command \"%s\" not found.\n", command_name);
            continue;
        }
        int ret = commands[command_id].handler(command_str);
        if (ret == CMD_INVALID_ARGUMENT) {
            fprintf(stderr, "Invalid argument.\n");
        }
        if (ret == CMD_INVALID_INDEX) {
            fprintf(stderr, "Index out of bound.\n");
        }
    }
    
    return 0;
}
