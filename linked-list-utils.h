#ifndef __LINKED_LIST_UTILS_H__
#define __LINKED_LIST_UTILS_H__

#include "linked-list.h"
#include <stdbool.h>

typedef void (foreach_callback)(int);
void foreach(struct linked_list_node* head, foreach_callback* callback) ;

typedef int (map_callback)(int);
struct linked_list_node* map(struct linked_list_node* head, map_callback* callback);

struct linked_list_node* map_mut(struct linked_list_node* head, map_callback* callback);

typedef int (foldl_callback)(int, int);
int foldl(struct linked_list_node* head, int init_value, foldl_callback* callback);

typedef int (iterate_callback)(int);
struct linked_list_node* iterate(size_t n, int init_value, iterate_callback* callback);

// my additional functions
typedef void (foreach_with_callback)(int, void*);
void foreach_with(struct linked_list_node* head, void* data, foreach_with_callback* callback);

typedef int (map_with_callback)(int, void*);
struct linked_list_node* map_mut_with(struct linked_list_node* head, void* data, map_with_callback* callback);

typedef int (generate_callback)(void*, bool*);
struct linked_list_node* generate(void* data, generate_callback* callback);

bool save(struct linked_list_node* head, const char* filename);
bool load(struct linked_list_node** head, const char* filename);
bool serialize(struct linked_list_node* head, const char* filename);
bool deserialize(struct linked_list_node** head, const char* filename);

#endif
