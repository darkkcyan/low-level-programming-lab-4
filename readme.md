# Project structure
- `linked-list.*` declares the linked list data structure with some basic functions.
- `linked-list-utils.*` declares the helper functions for the linked list 
with higher order functions.
- `callback-utils.*` - declares some small functions that can be used as parameter for the 
functions in `linked-list-utils.*`. Even some parts in `linked-list-utils` used these functions.
- `main.c` - defines commands and the logics for interactive interface.

# Build
```
make
```
After this the executable file `./main` can be launched.

# List of commands
Just launch `./main` and type `help<ENTER>`.
