#include "linked-list.h"
#include <stdlib.h>

struct linked_list_node* list_create(int value) {
    struct linked_list_node* head = malloc(sizeof(struct linked_list_node));
    head->next = NULL;
    head->value = value;
    return head;
}

void list_add_front(struct linked_list_node** head, int value) {
    if (!*head) {
        *head = list_create(value);
        return ;
    }
    struct linked_list_node *tail = *head;
    while (tail->next) {
        tail = tail->next;
    }
    tail->next = list_create(value);
}

void list_add_back(struct linked_list_node** head, int value) {
    struct linked_list_node* res = list_create(value);
    res->next = *head;
    *head = res;
}

size_t list_length(struct linked_list_node* head) {
    size_t res = 0;
    while (head) {
        ++res;
        head = head->next;
    }
    return res;
}

struct linked_list_node* list_node_at(struct linked_list_node* head, size_t index) {
    size_t length = list_length(head);
    if (index >= length) {
        return NULL;
    }
    size_t rev_index = length  - index - 1;
    while (rev_index --) {
        head = head->next;
    }
    return head;
}

int list_get(struct linked_list_node* head, size_t index) {
    head = list_node_at(head, index);
    return !head ? 0 : head->value;
}

void list_free(struct linked_list_node* head) {
    struct linked_list_node* temp;
    while (head) {
        temp = head;
        head = head->next;
        free(temp);
    }
}

int list_sum(struct linked_list_node* head) {
    int sum = 0;
    for (; head; head = head->next) {
        sum += head->value;
    }
    return sum;
}
