#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__

#include <stddef.h>  // for size_t type

struct linked_list_node {
    struct linked_list_node* next;
    int value;
};

struct linked_list_node* list_create(int value);
void list_add_front(struct linked_list_node** head, int value);
void list_add_back(struct linked_list_node** head, int value);
size_t list_length(struct linked_list_node* head);
int list_get(struct linked_list_node* head, size_t index);
struct linked_list_node* list_node_at(struct linked_list_node* head, size_t index);
void list_free(struct linked_list_node* head);
int list_sum(struct linked_list_node* head);

#endif
