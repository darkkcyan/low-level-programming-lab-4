#include "linked-list-utils.h"
#include "callback-utils.h"
#include <stdio.h>

void foreach(struct linked_list_node* head, foreach_callback* callback) {
    if (!head) {
        return ;
    }
    foreach(head->next, callback);
    callback(head->value);
}

struct linked_list_node* map(struct linked_list_node* head, map_callback* callback) {
    if (!head) {
        return NULL;
    }
    struct linked_list_node* res = map(head->next, callback);
    list_add_back(&res, callback(head->value));
    return res;
}

struct linked_list_node* map_mut(struct linked_list_node* head, map_callback* callback) {
    if (!head) {
        return NULL;
    }
    map_mut(head->next, callback);
    head->value = callback(head->value);
    return head;
}

int foldl(struct linked_list_node* head, int init_value, foldl_callback* callback) {
    if (!head) {
        return init_value;
    }
    return callback(foldl(head->next, init_value, callback), head->value);
}

struct linked_list_node* iterate(size_t n, int init_value, iterate_callback* callback) {
    if (!n--) {
        return NULL;
    }
    struct linked_list_node* head = list_create(init_value);
    while (n--) {
        list_add_back(&head, callback(head->value));
    }
    return head;
}

void foreach_with(struct linked_list_node* head, void* data, foreach_with_callback* callback) {
    if (!head) {
        return ;
    }
    foreach_with(head->next, data, callback);
    callback(head->value, data);
}

struct linked_list_node* map_mut_with(struct linked_list_node* head, void* data, map_with_callback* callback) {
    if (!head) {
        return NULL;
    }
    map_mut_with(head->next, data, callback);
    head->value = callback(head->value, data);
    return head;
}

struct linked_list_node* generate(void* data, generate_callback* callback) {
    bool stop = false;
    struct linked_list_node* res = NULL;
    do {
        int new_val = callback(data, &stop);
        if (stop) break;
        list_add_back(&res, new_val);
    } while (true);
    return res;
}

bool save(struct linked_list_node* head, const char* filename) {
    FILE* file = fopen(filename, "w");
    if (!file) return false;
    foreach_with(head, file, write_int_to_text_file);
    int has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool load(struct linked_list_node** head, const char* filename) {
    FILE* file = fopen(filename, "r");
    if (!file) return false;
    *head = generate(file, read_int_from_text_file);
    int has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool serialize(struct linked_list_node* head, const char* filename) {
    FILE* file = fopen(filename, "wb");
    if (!file) return false;
    foreach_with(head, file, serialize_int_to_file);
    int has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool deserialize(struct linked_list_node** head, const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (!file) return false;
    *head = generate(file, deserialize_int_from_file);
    int has_error = ferror(file);
    fclose(file);
    return !has_error;
}
