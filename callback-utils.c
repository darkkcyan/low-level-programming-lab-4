#include "callback-utils.h"
#include <stdio.h>
#include <stdbool.h>

void write_int_to_text_file(int num, void* text_file) {
    fprintf((FILE*) text_file, "%d ", num);
}

int read_int_from_text_file(void* text_file, bool* stop) {
    int res;
    *stop = fscanf((FILE*)text_file, "%d", &res) != 1;
    return res;
}

void serialize_int_to_file(int num, void* file) {
    fwrite(&num, sizeof(num), 1, (FILE*) file);
}

int deserialize_int_from_file(void* file, bool* stop) {
    int res;
    *stop = fread(&res, sizeof(res), 1, (FILE*) file) != 1;
    return res;
}
